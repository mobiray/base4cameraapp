﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RawImage))]
[RequireComponent(typeof(AspectRatioFitter))]
public class CameraLoader2 : MonoBehaviour
{
    public Text DebugText;

    private WebCamTexture webCamTexture;
    private string yourPath;

    private RawImage img;
    private AspectRatioFitter rawImageARF;
    private WebCamDevice[] dvcs;
    private int currentDvc = 0;

    static public CameraLoader2 Instance { get { return _instance; } }
    static protected CameraLoader2 _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        dvcs = WebCamTexture.devices;

        img = this.GetComponent<RawImage>();
        rawImageARF = gameObject.GetComponent<AspectRatioFitter>();
        yourPath = Application.persistentDataPath;

        CreateWebCamTexture();

        /*---UpdateCode---*/
        //float videoRatio = (float)webCamTexture.width / webCamTexture.height;
        //rawImageARF.aspectRatio = videoRatio;
        rawImageARF.aspectRatio = (float)webCamTexture.width / webCamTexture.height;
    }

    public void TakeMyPhoto()
    {
        StartCoroutine(TakePhotoCo());
    }

    public IEnumerator TakePhotoCo()
    {
        yield return new WaitForEndOfFrame();
        Color[] photoPixels = webCamTexture.GetPixels();

        Texture2D photo = new Texture2D(webCamTexture.height, webCamTexture.width);
        Color[] transpodePhotoPixels = new Color[photoPixels.Length];
        /*Seems that it is possible to simplify without using transpodePhotoPixels array*/
        int widthValue = 0;
        if (!dvcs[currentDvc].isFrontFacing)
        {
            /*If main camera should flip image horizontally*/
            widthValue = webCamTexture.width - 1;
        }
        for (int i = 0; i < webCamTexture.height; i++)
        {
            for (int j = 0; j < webCamTexture.width; j++)
            {
                transpodePhotoPixels[Mathf.Abs(widthValue - j) * webCamTexture.height + i] = photoPixels[j + i * webCamTexture.width];
            }
        }
        photo.SetPixels(transpodePhotoPixels);
        photo.Apply();

        byte[] bytes = photo.EncodeToPNG();

        string dateTime = System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();
        dateTime = "Photo" + dateTime + ".png";
        File.WriteAllBytes(yourPath + dateTime, bytes);
    }

    public void ChangeDevice()
    {
        StartCoroutine(ChangeDeviceCo());
    }

    public IEnumerator ChangeDeviceCo()
    {
        yield return new WaitForEndOfFrame();
        currentDvc = (currentDvc + 1) % dvcs.Length;
        webCamTexture.Stop();
        CreateWebCamTexture();
    }

    public void CreateWebCamTexture()
    {
        if (dvcs[currentDvc].isFrontFacing)
        {
            img.uvRect = new Rect(1, 0, -1, 1);  // means flip on vertical axis
        } else
        {
            img.uvRect = new Rect(0, 0, 1, 1);  // means no flip
        }

        webCamTexture = new WebCamTexture(dvcs[currentDvc].name, Screen.currentResolution.height, Screen.currentResolution.width);
        img.texture = webCamTexture;
        img.material.mainTexture = webCamTexture;
        webCamTexture.Play();
        DebugText.text = "camW = " + (float)webCamTexture.width + ", camH = " + (float)webCamTexture.height;
        //DebugText.text = "hasVibrator? = " + CustomVibrator.HasVibrator();
    }

    public void DeleteWebCamTexture()
    {
        webCamTexture.Stop();
    }
}