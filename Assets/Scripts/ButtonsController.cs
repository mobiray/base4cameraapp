﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsController : MonoBehaviour {


    public void OnSnapshotButtonClick()
    {
        CameraLoader2.Instance.TakeMyPhoto();
    }

    public void OnChangeDeviceButtonClick()
    {
        CameraLoader2.Instance.ChangeDevice();
    }

    public void OnVibrateButtonClick()
    {
        CustomVibrator.Vibrate(200);
    }

    public void OnToggleCameraClick()
    {
        if (this.GetComponent<Toggle>().isOn)
        {
            Debug.logger.Log("MyLOG", "camera is turned on");
            CameraLoader2.Instance.CreateWebCamTexture();
        } else
        {
            Debug.logger.Log("MyLOG", "camera is turned off");
            CameraLoader2.Instance.DeleteWebCamTexture();
        }
    }
}
