﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private WebCamTexture mCamera;

	void Start ()
    {
        mCamera = new WebCamTexture(1920, 1080);
        this.GetComponent<Renderer>().material.mainTexture = mCamera;
        mCamera.Play();	
	}

}
