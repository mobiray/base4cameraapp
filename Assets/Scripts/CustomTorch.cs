﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomTorch : MonoBehaviour {

    private AndroidJavaObject customCamera = null;
    private AndroidJavaObject cameraParameters = null;


    private string FLASH_MODE = "torch";

    private void Start()
    {
        
    }

    public void ToggleTorch()
    {
        Debug.logger.Log("MyLOG", "On Toggle torch click");
        if (customCamera == null)
        {
            Debug.logger.Log("MyLOG", "custom camera is null");
            AndroidJavaClass cameraClass = new AndroidJavaClass("android.hardware.Camera");
            customCamera = cameraClass.CallStatic<AndroidJavaObject>("open", 0);
            if(customCamera != null)
            {
                Debug.logger.Log("MyLOG", "custom camera is not null");
                cameraParameters = customCamera.Call<AndroidJavaObject>("getParameters");
                cameraParameters.Call("setFlashMode", FLASH_MODE);
                customCamera.Call("setParameters", cameraParameters);
                Debug.logger.Log("MyLOG", "Camera parameters was set");
            }
        } else
        {
            cameraParameters = customCamera.Call<AndroidJavaObject>("getParameters");
            string flashmode = cameraParameters.Call<string>("getFlashMode");
            if (flashmode != "torch")
            {
                Debug.logger.Log("MyLOG", "flashmode != torch");
                cameraParameters.Call("setFlashMode", FLASH_MODE);
                Debug.logger.Log("MyLOG", "Camera parameters was set");
            } else
            {
                Debug.logger.Log("MyLOG", "flashmode == torch");
                cameraParameters.Call("setFlashMode", "off");
                Debug.logger.Log("MyLOG", "Camera parameters was set");
            }
            customCamera.Call("setParameters", cameraParameters);
        }
    }

    private void ReleaseAndroidJavaObjects()
    {
        if (customCamera != null)
        {
            customCamera.Call("release");
            customCamera = null;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            Debug.logger.Log("MyLOG", "application was paused");
            ReleaseAndroidJavaObjects();
        } else
        {
            Debug.logger.Log("MyLOG", "application was started");
        }
    }

}
