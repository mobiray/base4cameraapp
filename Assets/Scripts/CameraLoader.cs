﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class CameraLoader : MonoBehaviour
{
    public Text tmpText;

    WebCamTexture webCamTexture;
    string yourPath;
    RawImage rawImage;
    AspectRatioFitter rawImageARF;

    void Start()
    {
        rawImage = gameObject.GetComponent<RawImage>();
        rawImageARF = gameObject.GetComponent<AspectRatioFitter>();
        yourPath = Application.persistentDataPath;
        Debug.Log(yourPath);
        webCamTexture = new WebCamTexture(1920, 1080);
        GetComponent<RawImage>().texture = webCamTexture;
        GetComponent<RawImage>().material.mainTexture = webCamTexture;
        //GetComponent<Renderer>().material.mainTexture = webCamTexture;
        webCamTexture.anisoLevel = 9;
        webCamTexture.Play();
        tmpText.text = "camW = " + (float)webCamTexture.width + ", camH = " + (float)webCamTexture.height;
    }

    public void TakeMyPhoto()
    {
        StartCoroutine(TakePhoto());
    }

    IEnumerator TakePhoto()
    {
        yield return new WaitForEndOfFrame();

        Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
        photo.SetPixels(webCamTexture.GetPixels());
        photo.Apply();

        byte[] bytes = photo.EncodeToPNG();
        string dateTime = System.DateTime.Now.Year.ToString()+ System.DateTime.Now.Month.ToString()+ System.DateTime.Now.Day.ToString()+ System.DateTime.Now.Hour.ToString()+ System.DateTime.Now.Minute.ToString()+ System.DateTime.Now.Second.ToString();
        dateTime = "Photo" + dateTime + ".png";
        File.WriteAllBytes(yourPath + dateTime, bytes);
    }

    private void Update()
    {
      /*  if (webCamTexture.width < 100)
        {
            Debug.Log("Still waiting another frame for correct info...");
            return;
        }

        // change as user rotates iPhone or Android:

        int cwNeeded = webCamTexture.videoRotationAngle;
        // Unity helpfully returns the _clockwise_ twist needed
        // guess nobody at Unity noticed their product works in counterclockwise:
        int ccwNeeded = -cwNeeded;

        // IF the image needs to be mirrored, it seems that it
        // ALSO needs to be spun. Strange: but true.
        if (webCamTexture.videoVerticallyMirrored) ccwNeeded += 180;

        // you'll be using a UI RawImage, so simply spin the RectTransform
        //rawImageRT.localEulerAngles = new Vector3(0f, 0f, ccwNeeded);
        */
        float videoRatio = (float)webCamTexture.width / (float)webCamTexture.height;

        // you'll be using an AspectRatioFitter on the Image, so simply set it
        rawImageARF.aspectRatio = videoRatio;
        
        // alert, the ONLY way to mirror a RAW image, is, the uvRect.
        // changing the scale is completely broken.
    /*    if (webCamTexture.videoVerticallyMirrored)
            rawImage.uvRect = new Rect(1, 0, -1, 1);  // means flip on vertical axis
        else
            rawImage.uvRect = new Rect(0, 0, 1, 1);  // means no flip
*/
        // devText.text =
        //  videoRotationAngle+"/"+ratio+"/"+wct.videoVerticallyMirrored;
    }
}